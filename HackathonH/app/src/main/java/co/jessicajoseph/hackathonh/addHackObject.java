package co.jessicajoseph.hackathonh;

/**
 * Created by Jess on 7/23/15.
 */
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class addHackObject extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_hack_object);

        final Button switchact =(Button)findViewById(R.id.switch_back_button);
        switchact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent act1 = new Intent(view.getContext(),MainActivity.class);
                startActivity(act1);

            }
        });
    }

    public void addHackObject(){
        HackathonObject newObject = new HackathonObject(hackName(),hackPhoto(),hackWeb(),hackMonth(),hackDay(),hackYear(),hackCity(),hackState());
    }

            private String hackName(){
                TextView hackNameView = (TextView) findViewById(
                        R.id.hack_name_text_view);

                return hackNameView.getText().toString();
            }

            private String hackPhoto(){
                TextView hackPhotoView = (TextView) findViewById(
                        R.id.hack_photo_text_view);

                return hackPhotoView.getText().toString();
            }

            private String hackWeb(){
                TextView hackWebView = (TextView) findViewById(
                        R.id.hack_web_text_view);

                return hackWebView.getText().toString();
            }

            private Integer hackMonth(){
                TextView hackMonthView = (TextView) findViewById(
                        R.id.hack_month_text_view);

                return Integer.valueOf(hackMonthView.getText().toString());
            }

            private Integer hackDay(){
                TextView hackDayView = (TextView) findViewById(
                        R.id.hack_day_text_view);

                return Integer.valueOf(hackDayView.getText().toString());
            }

            private Integer hackYear(){
                TextView hackYearView = (TextView) findViewById(
                        R.id.hack_year_text_view);

                return Integer.valueOf(hackYearView.getText().toString());
            }

            private String hackCity(){
                TextView hackCityView = (TextView) findViewById(
                        R.id.hack_city_text_view);

                return hackCityView.getText().toString();
            }

            private String hackState(){
                TextView hackStateView = (TextView) findViewById(
                        R.id.hack_state_text_view);

                return hackStateView.getText().toString();
            }

}


