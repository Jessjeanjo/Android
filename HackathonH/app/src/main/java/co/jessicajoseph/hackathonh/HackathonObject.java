package co.jessicajoseph.hackathonh;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.sql.Array;
import java.util.Calendar;

/**
 * Created by Jess on 7/23/15.
 */
public class HackathonObject extends DialogFragment implements DatePickerDialog.OnDateSetListener{
    private String name, website;
    private Integer[] date;
    private String[] location;


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day){

        }


    //Constructor
    public HackathonObject(String hName, String hWeb, Integer month,
                           Integer day,Integer year, String city, String state){

        name = hName;
//        displayPhoto = hPhoto;
        website = hWeb;

        date = new Integer[3];
        date[0] = month; date[1] = day; date[2] = year;

        location = new String[2];
        location[0] = city; location[1] = state;

    }


    //Accessors
    public String getName(){return name;}
//    public String getDisplayPhoto(){return displayPhoto;}
    public String getWebsite(){return website;}
    public Integer[] getDate(){return date;}
    public String[] getLocation(){return location;}

    //Mutators
    public void changeName(String newName){name = newName;}
//    public void changeDisplayPhoto(String newPhoto){ displayPhoto = newPhoto;}
    public void changeWebsite(String newWeb){ website = newWeb;}
    public void changeMonth(Integer newMon){ date[0] = newMon;}
    public void changeDay(Integer newDay){ date[1] = newDay;}
    public void changeYear(Integer newYear){ date[2] = newYear;}
    public void changeCity(String newCity){ location[0] = newCity;}
    public void changeState(String newState){ location[1] = newState;}


}
